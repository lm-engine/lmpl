#pragma once

namespace lmpl
{
enum class key_code
{
    A = 4,
    B = 5,
    C = 6,
    D = 7,
    E = 8,
    F = 9,
    G = 10,
    H = 11,
    I = 12,
    J = 13,
    K = 14,
    L = 15,
    M = 16,
    N = 17,
    O = 18,
    P = 19,
    Q = 20,
    R = 21,
    S = 22,
    T = 23,
    U = 24,
    V = 25,
    W = 26,
    X = 27,
    Y = 28,
    Z = 29,
    KEY_1 = 30,
    KEY_2 = 31,
    KEY_3 = 32,
    KEY_4 = 33,
    KEY_5 = 34,
    KEY_6 = 35,
    KEY_7 = 36,
    KEY_8 = 37,
    KEY_9 = 38,
    KEY_0 = 39,
    Enter = 40,
    Escape = 41,
    Delete = 42,
    Tab = 43,
    Space = 44,
    Minus = 45,
    Equals = 46,
    LeftBracket = 47,
    RightBracket = 48,
    Backslash = 49,
    Semicolon = 51,
    Quote = 52,
    Grave = 53,
    Comma = 54,
    Period = 55,
    Slash = 56,
    CapsLock = 57,
    F1 = 58,
    F2 = 59,
    F3 = 60,
    F4 = 61,
    F5 = 62,
    F6 = 63,
    F7 = 64,
    F8 = 65,
    F9 = 66,
    F10 = 67,
    F11 = 68,
    F12 = 69,
    PrintScreen = 70,
    ScrollLock = 71,
    Pause = 72,
    Insert = 73,
    Home = 74,
    PageUp = 75,
    DeleteForward = 76,
    End = 77,
    PageDown = 78,
    Right = 79,
    Left = 80,
    Down = 81,
    Up = 82,
    NUM_NumLock = 83,
    NUM_Divide = 84,
    NUM_Multiply = 85,
    NUM_Subtract = 86,
    NUM_Add = 87,
    NUM_Enter = 88,
    NUM_1 = 89,
    NUM_2 = 90,
    NUM_3 = 91,
    NUM_4 = 92,
    NUM_5 = 93,
    NUM_6 = 94,
    NUM_7 = 95,
    NUM_8 = 96,
    NUM_9 = 97,
    NUM_0 = 98,
    NUM_Point = 99,
    NonUSBackslash = 100,
    NUM_Equals = 103,
    F13 = 104,
    F14 = 105,
    F15 = 106,
    F16 = 107,
    F17 = 108,
    F18 = 109,
    F19 = 110,
    F20 = 111,
    F21 = 112,
    F22 = 113,
    F23 = 114,
    F24 = 115,
    Help = 117,
    Menu = 118,
    LeftControl = 224,
    LeftShift = 225,
    LeftAlt = 226,
    LeftGUI = 227,
    RightControl = 228,
    RightShift = 229,
    RightAlt = 230,
    RightGUI = 231,
    n_keys
};

inline std::map<key_code, std::string> key_code_name_map{
  {key_code::A, "A"},     {key_code::B, "B"},     {key_code::C, "C"},
  {key_code::D, "D"},     {key_code::E, "E"},     {key_code::F, "F"},
  {key_code::G, "G"},     {key_code::H, "H"},     {key_code::I, "I"},
  {key_code::J, "J"},     {key_code::K, "K"},     {key_code::L, "L"},
  {key_code::M, "M"},     {key_code::N, "N"},     {key_code::O, "O"},
  {key_code::P, "P"},     {key_code::Q, "Q"},     {key_code::R, "R"},
  {key_code::S, "S"},     {key_code::T, "T"},     {key_code::U, "U"},
  {key_code::V, "V"},     {key_code::W, "W"},     {key_code::X, "X"},
  {key_code::Y, "Y"},     {key_code::Z, "Z"},     {key_code::KEY_1, "1"},
  {key_code::KEY_2, "2"}, {key_code::KEY_3, "3"}, {key_code::KEY_4, "4"},
  {key_code::KEY_5, "5"}, {key_code::KEY_6, "6"}, {key_code::KEY_7, "7"},
  {key_code::KEY_8, "8"}, {key_code::KEY_9, "9"}, {key_code::KEY_0, "0"},
};

class key_state
{
    bool map[(size_t)key_code::n_keys] = {false};

  public:
    bool &operator[](key_code code) { return map[(unsigned)code]; }
    bool operator[](key_code code) const { return map[(unsigned)code]; }

    bool have_modifiers() const { return shift() || control(); }

    bool shift() const
    {
        return (*this)[key_code::LeftShift] || (*this)[key_code::RightShift];
    }

    bool control() const
    {
        return (*this)[key_code::LeftControl] ||
               (*this)[key_code::RightControl];
    }

    bool alt() const
    {
        return (*this)[key_code::LeftAlt] || (*this)[key_code::RightAlt];
    }
};
}